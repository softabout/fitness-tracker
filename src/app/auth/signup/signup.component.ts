import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  minAge: any;

  constructor() { }

  ngOnInit(): void {
    this.minAge = new Date();
    this.minAge.setFullYear(this.minAge.getFullYear() - 18);
  }

  onSubmit(f: NgForm) {
    console.log(f);
    console.log(f.controls['email'].value)
  }
}
